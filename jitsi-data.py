import json
import logging
from elasticsearch import Elasticsearch
from datetime import datetime  # to specify a specific timeframe
from gender_guesser import detector as gender
from perceval.backends.core.git import Git
from dateutil.parser import parse
import time
from flask import Flask, render_template
import ndjson
import requests

app = Flask(__name__)

def clean_name(name):
    """
        Extract accent marks from a given name and capitalize it
    """
    name = name.lower()
    name = name.replace("á", "a")
    name = name.replace("é", "e")
    name = name.replace("í", "i")
    name = name.replace("ó", "o")
    name = name.replace("ú", "u")
    name = name.capitalize()
    return name

def detect_gender(name):
    """
        Detect gender from a given name after processing it
    """
    # Creation of a detector method to assign genre to authors of commits
    detector = gender.Detector()
    # Remove hyphen symbol and numbers from the name
    if "-" in name:
        name = name.split('-')[0]
    name = ''.join([i for i in name if not i.isdigit()])
    # Method call to genre detector library
    gender_detected = detector.get_gender(name)
    # Only set unknown, male or female (remove other genres)
    if gender_detected not in ("unknown", "male", "female"):
        gender_detected = "unknown"
    return gender_detected

def extract_data(data_repo, data_file, elastic, index_name):
    """
        Extract jitsi repo data and post it into ElasticSearch
    """
    # Creation of a structure to save data into dict and then into json file
    items = []
    index = 1
    for item in data_repo.fetch(from_date=datetime.strptime('2019-01-01', '%Y-%m-%d')):
        items.append(item)
        item['data']['email'] = item['data']['Author'].split(' ')[-1]
        item['data']['organization'] = item['data']['Author'].split(' ')[-1].split('@')[-1].split('.')[0]
        item['data']['Author'] = ' '.join(item['data']['Author'].split(' ')[:-1])
        try:
            author_name = item['data']['Author'].split(' ')[0]
            if author_name in ('Дамян', 'damencho', 'Damencho'):
                item['data']['gender'] = "male"
                item['data']['Author'] = "Damian Minkov"
            elif author_name == "Bettenbuk":
                item['data']['gender'] = "male"
            else:
                # Method call to clean name
                author_name = clean_name(author_name)
                # Method call to clean name
                item['data']['gender'] = detect_gender(author_name)
        except KeyError:
            logging.info(item['data']['Author'] + ' - email:' + item['data']['email'])
        # Method call to transform item to desired format
        elk_item = transform_item(item)
        # Save data into json and put it on ElasticSearch database
        json_data = json.dumps(elk_item)
        data_file.write(json_data + '\n')
        elastic.index(index=index_name, id=index, body=json_data)
        index = index + 1
    print('Commits: {}'.format(len(items)))

def transform_item(item):
    """
        Transform item data into a new structure matching the mapping for ElasticSearch
    """
    elk_item = dict()
    elk_item['backend_name'] = item['backend_name']
    elk_item['backend_version'] = item['backend_version']
    elk_item['perceval_version'] = item['perceval_version']
    elk_item['timestamp'] = datetime.fromtimestamp(item['timestamp']).strftime("%Y-%m-%d %H:%M:%S")
    elk_item['origin'] = item['perceval_version']
    elk_item['uuid'] = item['uuid']
    elk_item['updated_on'] = datetime.fromtimestamp(item['updated_on']).strftime("%Y-%m-%d %H:%M:%S")
    elk_item['category'] = item['category']
    elk_item['tag'] = item['tag']
    elk_item['commit_data'] = item['data']['commit']
    elk_item['parents_data'] = item['data']['parents']
    elk_item['author_requested_commit_data'] = item['data']['Author']  # ' '.join(item['data']['Author'].split(' ')[:-1])
    elk_item['author_date_data'] = parse(item['data']["AuthorDate"]).strftime("%Y-%m-%d %H:%M:%S")
    elk_item['author_accepted_commit_data'] = item['data']['Commit']
    elk_item['commit_date_data'] = parse(item['data']["CommitDate"]).strftime("%Y-%m-%d %H:%M:%S")
    elk_item['offset_utc_data'] = parse(item['data']["AuthorDate"]).strftime("%z").rstrip("0")
    if elk_item['offset_utc_data'] == "+":
        elk_item['offset_utc_data'] = "0"
    if "53" in elk_item['offset_utc_data']:
        elk_item['offset_utc_data'] = elk_item['offset_utc_data'].replace("53", "5")
    elk_item['message_data'] = item['data']['message']
    elk_item['gender_data'] = item['data']['gender']

    # Method call to get modified lines
    lines_added, lines_removed = get_lines_modified(item)
    elk_item['lines_added_data'] = lines_added
    elk_item['lines_removed_data'] = lines_removed
    elk_item['email_data'] = item['data']['email']
    elk_item['organization_data'] = item['data']['organization']
    return elk_item

def get_lines_modified(item):
    """
        Logic for calculate the number of lines modified (added and removed)
    """
    lines_added = 0
    lines_removed = 0
    for file in item['data']['files']:
        if 'added' in file:
            try:
                lines_added += int(file['added'])
            except ValueError:
                print("Valor no numerico: "+file['added'])
        if 'removed' in file:
            try:
                lines_removed += int(file['removed'])
            except ValueError:
                print("Valor no numerico: "+file['removed'])
    return lines_added, lines_removed

@app.route('/')
def index_page():
    return render_template('index.html')

@app.route('/start')
def start():
    # Instance of elastic search to put the data into ElasticSearch/Kibana system (it is necessary to have both up on localhost)
    elastic = Elasticsearch(['http://es01:9200/'], verify_certs=True)

    while not elastic.ping():
        time.sleep(5)

    # Read mapping json file to ElasticSearch
    mapping_file = open("mapping.json", "r")
    mapping = json.load(mapping_file)
    # Creation of ElasticSearch index using the mapping
    index_name = "repo_jitsi_index_final"
    response = elastic.indices.create(
        index=index_name,
        body=mapping,
        ignore=400
    )
    # Instance to log events on python terminal
    logging.basicConfig(level=logging.INFO)
    # It has 2 arguments, uri and gitpath: repo = Git(uri=repository, gitpath='/tmp/'+repository.git)
    data_repo = Git(uri='https://github.com/jitsi/jitsi-meet', gitpath='/tmp/jitsi-meet.git')
    # Open in write mode a json document to save data from repository
    data_file = open("jitsi_git_data.json", "w")
    # Method call to extract data from jitsi repository, process it and post it to ElasticSearch
    extract_data(data_repo, data_file, elastic, index_name)

    time.sleep(5)

    # Read dashboard json file for Kibana
    headers = {'kbn-xsrf': 'true'}
    r = requests.post('http://kib01:5601/api/saved_objects/_import?overwrite=true', headers=headers, files={'file':open('impact_dashboard.ndjson')})

    return render_template('final.html')
